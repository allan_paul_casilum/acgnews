<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function acg_admin_url($uri = '') {
	return admin_url('admin.php?page=IssueSettings' . $uri);
}
function acg_public_url($uri = '') {
	return home_url($uri);
}
function acg_redirect_to($url) {
	?>
	<script type="text/javascript">
		window.location = '<?php echo $url; ?>';
	</script>
	<?php
	die();
}

function allteams_query_post_issue_number_db()
{
	global $wpdb;

	$meta_key = 'issue-number';
	$sql = "
	SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
	LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
	WHERE pm.meta_key = '%s'
	AND p.post_status IN ('publish', 'draft')
	ORDER BY pm.meta_value";

	$result = $wpdb->get_col( 
		$wpdb->prepare( $sql, $meta_key) 
	);

	return $result;
}

function allteams_query_db_issue_number()
{
	//SELECT DISTINCT pm.meta_value FROM wp_postmeta pm WHERE pm.meta_key = 'number' ORDER BY pm.meta_value
	global $wpdb;

	$meta_key = 'number';
	$sql = "
	SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
	WHERE pm.meta_key = '%s'
	ORDER BY pm.meta_value";

	$result = $wpdb->get_col( 
		$wpdb->prepare( $sql, $meta_key) 
	);

	return $result;
}