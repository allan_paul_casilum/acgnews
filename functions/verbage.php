<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
function acg_verbage($key) {
	$text_domain = acg_get_text_domain();

	$arr = [
		'settings_menu' => [
			'page_title' => __('Issues', $text_domain),
			'menu_title' => __('Issues', $text_domain),
		],
		'issues' => [
			'singular' => __('Issue', $text_domain),
			'plural' => __('Issues', $text_domain)
		]
	];

	$arr = apply_filters( 'acg_verbage', $arr );

	return isset($arr[$key]) ? $arr[$key] : '';
}
