<?php

/**
 * Fired during plugin activation
 *
 * @link       spacific.com
 * @since      1.0.0
 *
 * @package    Acgnews
 * @subpackage Acgnews/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Acgnews
 * @subpackage Acgnews/includes
 * @author     Spacific <info@spacific.com>
 */
class Acgnews_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
