<?php

/**
 * Fired during plugin deactivation
 *
 * @link       spacific.com
 * @since      1.0.0
 *
 * @package    Acgnews
 * @subpackage Acgnews/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Acgnews
 * @subpackage Acgnews/includes
 * @author     Spacific <info@spacific.com>
 */
class Acgnews_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
