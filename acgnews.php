<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              spacific.com
 * @since             1.0.0
 * @package           Acgnews
 *
 * @wordpress-plugin
 * Plugin Name:       Site News
 * Plugin URI:        allteams.com
 * Description:       Site News.
 * Version:           1.0.0
 * Author:            Spacific
 * Author URI:        spacific.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       acgnews
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('ACG_VERSION', '1.0.0' );
define('ACG_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define('ACG_PARENT_MENU_URL', 'IssueSettings' );
define('ACG_CPT_PREFIX', 'issue-settings' );
define('ACG_EVENTS_CPT_PREFIX', 'acg-events' );
define('ACG_ISSUE_META_PREFIX', 'issue-number' );
define('ACG_COOKIE_PREFIX', 'issue-number' );
define('ACG_CURRENT_ISSUE_COOKIE_PREFIX', 'current-issue-view' );
/**
 * For autoloading classes
 * */
spl_autoload_register('acg_directory_autoload_class');
function acg_directory_autoload_class($class_name){
		if ( false !== strpos( $class_name, 'ACG' ) ) {
	 $include_classes_dir = realpath( get_template_directory( __FILE__ ) ) . DIRECTORY_SEPARATOR;
	 $admin_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
	 $class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
	 if( file_exists($include_classes_dir . $class_file) ){
		 require_once $include_classes_dir . $class_file;
	 }
	 if( file_exists($admin_classes_dir . $class_file) ){
		 require_once $admin_classes_dir . $class_file;
	 }
 }
}
function acg_get_plugin_details(){
 // Check if get_plugins() function exists. This is required on the front end of the
 // site, since it is in a file that is normally only loaded in the admin.
 if ( ! function_exists( 'get_plugins' ) ) {
	 require_once ABSPATH . 'wp-admin/includes/plugin.php';
 }
 $ret = get_plugins();
 return $ret['acgnews/acgnews.php'];
}
function acg_get_text_domain(){
 $ret = acg_get_plugin_details();
 return $ret['TextDomain'];
}
function acg_get_plugin_dir(){
 return plugin_dir_path( __FILE__ );
}
require_once plugin_dir_path( __FILE__ ) . 'functions/helper.php';
require_once plugin_dir_path( __FILE__ ) . 'functions/verbage.php';
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-acgnews-activator.php
 */
function activate_acgnews() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-acgnews-activator.php';
	Acgnews_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-acgnews-deactivator.php
 */
function deactivate_acgnews() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-acgnews-deactivator.php';
	Acgnews_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_acgnews' );
register_deactivation_hook( __FILE__, 'deactivate_acgnews' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-acgnews.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_acgnews() {

	$plugin = new Acgnews();
	$plugin->run();
	
	ACG_Issues_Sessions::get_instance()->init();
	ACG_Issues_MetaBoxPost::get_instance();
	ACG_Issues_WP::get_instance();
	ACG_Issues_AJAX::get_instance();
	ACG_Issues_MetaBox::get_instance();
	ACG_Issues_Shortcode::get_instance();
	ACG_Issues_QuickEdit::get_instance();
	ACG_Events_MetaBox::get_instance();
	ACG_Events_Shortcode::get_instance();
	ACG_Issues_IssueNumberFilter::get_instance();
}
//run_acgnews();
add_action('plugins_loaded', 'run_acgnews');

function acg_init() {
	ACG_Issues_CPT::get_instance();
	//ACG_Issues_Sessions::get_instance()->init();
	//events
	ACG_Events_CPT::get_instance();
}
add_action( 'init', 'acg_init');

function acg_rest_api() {
	ACG_Issues_API::get_instance()->initRestAPI();
}
add_action( 'rest_api_init', 'acg_rest_api' );

function acg_run_issues( $query ) {
	global $posts;

	if(!is_admin()){
		if ( is_front_page() ) {
				$entity = new ACG_Issues_Entity;
				$issue_number = $entity->getDefaultIssueNumber();
				if( ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue() ) {
					$issue_number = ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue();
				}
				$meta_query = [
					[
						'key'     => 'issue-number',
						'value'   => $issue_number,
					]
				];
				$query->set('meta_query', $meta_query);
		}
	}

}
add_action( 'pre_get_posts', 'acg_run_issues' );
function my_page_template_redirect()
{
		if(
			isset($_GET['preview-issue'])
			&& $_GET['preview-issue'] == 1
		){
			if(
				isset($_GET['issue-id'])
				&& trim($_GET['issue-id']) != ''
			) {
				if(
					is_user_logged_in()
					&& ( current_user_can('editor') || current_user_can('administrator') )
				) {
					ACG_Issues_Sessions::get_instance()->setCurrentReaderIssue($_GET['issue-id']);
				}
				//wp_redirect( home_url('?issue-id=' + $_GET['issue-id'] + '&preview-issue=' + $_GET['preview-issue']) );
        //die;
			}
		}else{
			/*$entity = new ACG_Issues_Entity;
			$issue_number = $entity->getDefaultIssueNumber();
			ACG_Issues_Sessions::get_instance()->setCurrentReaderIssue($issue_number);*/
		}
		if(isset($_SERVER['QUERY_STRING'])) {
			$query_str = explode('=', $_SERVER['QUERY_STRING']);
			if(isset($query_str[0]) && $query_str[0] == 'issue') {
				ACG_Issues_Sessions::get_instance()->setCurrentReaderIssue($query_str[1]);
				wp_redirect( home_url() );
        die;
			}else{
				/*$entity = new ACG_Issues_Entity;
				$issue_number = $entity->getDefaultIssueNumber();
				ACG_Issues_Sessions::get_instance()->setCurrentReaderIssue($issue_number);*/
			}
		}
}
add_action( 'template_redirect', 'my_page_template_redirect' );

//https://rudrastyh.com/wordpress/custom-bulk-actions.html
//add_filter( 'bulk_actions-edit-post', 'allteams_my_bulk_actions' );
function allteams_my_bulk_actions( $bulk_array ) {
	$result = allteams_query_db_issue_number();
	foreach($result as $number) {
		$bulk_array['misha_set_issue_' . $number] = 'Set Post to  Issue ' . $number;
	}
	return $bulk_array;
}

