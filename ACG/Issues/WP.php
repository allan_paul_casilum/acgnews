<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_WP {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function admin_menu(){
		$t = acg_get_plugin_details();
		$name = acg_verbage('settings_menu');
		add_menu_page(
			$name['page_title'],
			$name['menu_title'],
			'manage_options',
			ACG_PARENT_MENU_URL,
			array(ACG_Issues_Controller::get_instance(), 'controller'),
			'dashicons-editor-table',
			3
		);
	}

	public function custom_columns($column, $post_id)
	{

	}

	public function __construct()
	{
		$this->admin_menu();
		add_action( 'manage_posts_custom_column' , array($this, 'custom_columns'), 10, 2 );
	}

}
