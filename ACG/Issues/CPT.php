<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_CPT {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function cpt() {
		$t = acg_get_plugin_details();
		$name = acg_verbage('settings_menu');
		$text_domain = acg_get_text_domain();
		$labels = array(
			'name'               => _x( 'Issue', 'post type general name', $text_domain ),
			'singular_name'      => _x( 'Issue', 'post type singular name', $text_domain ),
			'menu_name'          => _x( 'Issue', 'admin menu', $text_domain ),
			'name_admin_bar'     => _x( 'Issue', 'add new on admin bar', $text_domain ),
			'add_new'            => _x( 'Add New', ACG_CPT_PREFIX, $text_domain ),
			'add_new_item'       => __( 'Add New Issue', $text_domain ),
			'new_item'           => __( 'New Issue', $text_domain ),
			'edit_item'          => __( 'Edit Issue', $text_domain ),
			'view_item'          => __( 'View Issue', $text_domain ),
			'all_items'          => __( 'All Issue', $text_domain ),
			'search_items'       => __( 'Search Issue', $text_domain ),
			'parent_item_colon'  => __( 'Parent Issue:', $text_domain ),
			'not_found'          => __( 'No Issue found.', $text_domain ),
			'not_found_in_trash' => __( 'No Issue found in Trash.', $text_domain )
		);

		$args = array(
			'labels'             => $labels,
	    	'description'        => __( 'Description.', $text_domain ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => false,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => ACG_CPT_PREFIX ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => null,
			'menu_icon'					 => 'dashicons-editor-table',
			'supports' => 		array('title'),
		);

		register_post_type( ACG_CPT_PREFIX, $args );
	}

	public function __construct()
	{
		$this->cpt();
	}

}
