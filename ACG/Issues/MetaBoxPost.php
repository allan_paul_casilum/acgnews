<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_MetaBoxPost {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	/**
   * Meta box initialization.
   */
  public function init_metabox()
	{
      add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
      add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
  }
	/**
   * Adds the meta box.
   */
  public function add_metabox()
	{
		$text_domain = acg_get_text_domain();
		add_meta_box(
			'issue-admin-meta-box',
			__( 'Issue Meta', $text_domain ),
			array( $this, 'render_metabox' ),
			['issue-settings'],
			'advanced',
			'default'
		);

  }
	/**
   * Renders the meta box.
   */
  public function render_metabox( $post )
    {
        wp_nonce_field( 'issue_inner_custom_box', 'issue_inner_custom_box_nonce' );
        // update_post_meta( $post_id, 'number', $number );
        // update_post_meta( $post_id, 'name', $name );
        // update_post_meta( $post_id, 'date', $date );
        // update_post_meta( $post_id, 'publish', $publish );
        $options = new ACG_Issues_Options;
	    $get_default_issue = $options->default_issue_homepage('r', 0);
        
        $post_id = $post->ID;
        $data = [
            'number' => get_post_meta($post_id, 'number', true),
            'name' => get_post_meta($post_id, 'name', true),
            'date' => get_post_meta($post_id, 'date', true),
            'publish' => get_post_meta($post_id, 'publish', true),
            'is_default_issue_number' => ($get_default_issue == $post_id) ? 1:0
        ];

		ACG_View::get_instance()->admin_partials('partials/settings/metaboxform.php', $data);
    }

		/**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function save_metabox( $post_id, $post )
		{
      /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */
 
        // Check if our nonce is set.
        if ( ! isset( $_POST['issue_inner_custom_box_nonce'] ) ) {
            return $post_id;
        }
 
        $nonce = $_POST['issue_inner_custom_box_nonce'];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'issue_inner_custom_box' ) ) {
            return $post_id;
        }
 
        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'issue-settings' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }

        // Sanitize the user input.
        $number = sanitize_text_field( $_POST['number'] );
        $date = sanitize_text_field( $_POST['date'] );
        $publish = sanitize_text_field( $_POST['publish'] );
        $name = sanitize_text_field( $_POST['post_title'] );
        // 'number' => $data_post['number'],
        // 'name' => $data_post['name'],
        // 'date' => $data_post['date'],
        // 'publish' => $data_post['publish'],
        // Update the meta field.
        update_post_meta( $post_id, 'number', $number );
        update_post_meta( $post_id, 'name', $name );
        update_post_meta( $post_id, 'date', $date );
        update_post_meta( $post_id, 'publish', $publish );

        if($_POST['default_issue_query'] == 1) {
            $options = new ACG_Issues_Options;
            $options->default_issue_homepage('u', $post_id);
            //set cookies current chosen issue number
            $get_issue_number = get_post_meta($post_id, 'number', 1);
            ACG_Issues_Sessions::get_instance()->set($get_issue_number);
        }
        
  }

	public function __construct()
	{
		if ( is_admin() ) {
			add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    	}
	}

}

