<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_Controller extends ACG_Base {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function save_settings()
	{
		$data_post = $_POST;
		if($data_post){
			ACG_Settings::get_instance()->update($data_post['defaultLabel']);
		}
		$redirect_url = acg_admin_url();
		acg_redirect_to($redirect_url);
	}
	public function insert()
	{
		$data_post = $_POST;
		if(
			trim($data_post['date']) != ''
			|| trim($data_post['number']) != ''
		) {
			$wp_insert_post = [
				'post_title' => 'issue-' . $data_post['number'],
				'post_type' => ACG_CPT_PREFIX,
				'post_status' => 'publish',
				'meta_input' => [
					'number' => $data_post['number'],
					'name' => $data_post['name'],
					'date' => $data_post['date'],
					'publish' => $data_post['publish'],
				]
			];
			wp_insert_post($wp_insert_post);
		}

		$redirect_url = acg_admin_url();
		acg_redirect_to($redirect_url);
	}

	public function create()
	{
		$data = [];
		$data['action'] = acg_admin_url();
		$data['method'] = 'insert';
		ACG_View::get_instance()->admin_partials('partials/settings/create.php', $data);
	}

	public function update()
	{
		$data_post = $_POST;
		if(
			trim($data_post['date']) != ''
			|| trim($data_post['number']) != ''
		) {
			$wp_update_post = [
				'ID' => $data_post['ID'],
				'post_title' => 'issue-' . $data_post['number'],
				'post_type' => ACG_CPT_PREFIX,
				'meta_input' => [
					'number' => $data_post['number'],
					'name' => $data_post['name'],
					'date' => $data_post['date'],
					'publish' => $data_post['publish'],
				]
			];
			wp_update_post($wp_update_post);
		}

		$redirect_url = acg_admin_url();
		acg_redirect_to($redirect_url);
	}

	public function edit()
	{
		$data = [];
		if(
				isset($_GET['id'])
				&& $_GET['id'] != ''
		) {
			$api = new ACG_Issues_API;
			$post_id = $_GET['id'];
			$data['action'] = acg_admin_url();
			$data['method'] = 'update';
			$data['qData'] = $api->get(['id'=>$post_id]);

			ACG_View::get_instance()->admin_partials('partials/settings/edit.php', $data);
		}else{
			$redirect_url = acg_admin_url();
			acg_redirect_to($redirect_url);
		}
	}

	public function IssueSettings()
	{
		$data = [];
		$api = new ACG_Issues_API;
		$data['action'] = acg_admin_url();
		$data['method'] = 'insert';
		$latest_issue = $api->getLatestIssues();
		$data['latest_issue'] = $latest_issue;
		$data['latest_issue']['number'] = isset($latest_issue[0]['meta']['number'][0]) ? $latest_issue[0]['meta']['number'][0] : 0;
		$data['issues'] = $api->getIssues();
		$data['issue_default_label'] = ACG_Settings::get_instance()->get();

		ACG_View::get_instance()->admin_partials('partials/settings/index.php', $data);
	}

	/**
	 * Controller
	 *
	 * @param	$action		string | empty
	 * @parem	$arg		array
	 * 						optional, pass data for controller
	 * @return mix
	 * */
	public function controller($action = '', $arg = array()){
		$this->call_method($this, $action);
	}

	public function __construct(){}

}
