<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *https://generatewp.com/managing-content-easily-quick-edit/
 * @since 0.0.1
 * */
class ACG_Issues_QuickEdit {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function issues_quickedit_custom_posts_columns($posts_columns)
	{
		$text_domain = acg_get_text_domain();
		$posts_columns['issue_number'] = __( 'Issue Number', $text_domain );
    	return $posts_columns;
	}

	public function issues_quickedit_custom_column_display($column_name, $post_id)
	{
		if ( 'issue_number' == $column_name ) {
			$issue_entity = new ACG_Issues_Entity;
			$meta = $issue_entity->issue_number_meta(
				[
					'post_id' => $post_id,
					'action' => 'r',
					'single' => true
				]
			);
			$text_domain = acg_get_text_domain();
			if ( $meta ) {
				echo esc_html( $meta );
			} else {
				esc_html_e( 'N/A', $text_domain );
			}
    	}
	}


	public function issues_quickedit_fields($column_name, $post_type)
	{
		if ( 'issue_number' != $column_name )
        return;
		$text_domain = acg_get_text_domain();
		$get_issue_numbers = allteams_query_db_issue_number();
		?>
		<fieldset class="inline-edit-col-right">
			<div class="inline-edit-col">
				<label>
					<span class="title"><?php esc_html_e( 'Issue Number', $text_domain ); ?></span>
					<span class="input-text-wrap">
						<?php if($get_issue_numbers) { ?>
								<select name="issue-number" class="issue-number-select">
									<option value="0">Select Issue Number</option>
									<?php foreach($get_issue_numbers as $k => $v) { ?>
										<option value="<?php echo $v;?>">
											Issue - <?php echo $v;?>
										</option>
									<?php } ?>
								</select>
						<?php } ?>
					</span>
				</label>
			</div>
		</fieldset>
		<?php
	}

	public function issues_quickedit_set_data( $actions, $post ) {
		$issue_entity = new ACG_Issues_Entity;
		$meta = $issue_entity->issue_number_meta(
			[
				'post_id' => $post->ID,
				'action' => 'r',
				'single' => true
			]
		);
    if ( $meta ) {
        if ( isset( $actions['inline hide-if-no-js'] ) ) {
            $new_attribute = sprintf( 'data-edit-issue-number="%s"', esc_attr( $meta ) );
            $actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
        }
    }

    return $actions;
	}

	public function issues_quickedit_javascript()
	{
		$current_screen = get_current_screen();

		if ( $current_screen->id != 'edit-post' || $current_screen->post_type != 'post' )
			return;

		// Ensure jQuery library loads
		wp_enqueue_script( 'jquery' );
		?>
		<script type="text/javascript">
			jQuery( function( $ ) {
				// it is a copy of the inline edit function
				var wp_inline_edit_function = inlineEditPost.edit;

				// we overwrite the it with our own
				inlineEditPost.edit = function( post_id ) {

					// let's merge arguments of the original function
					wp_inline_edit_function.apply( this, arguments );

					// get the post ID from the argument
					var id = 0;
					if ( typeof( post_id ) == 'object' ) { // if it is object, get the ID number
						id = parseInt( this.getId( post_id ) );
					}

					//if post id exists
					if ( id > 0 ) {

						// add rows to variables
						var specific_post_edit_row = $( '#edit-' + id );
						var	specific_post_row = $( '#post-' + id );
						var	issue_number = $( '.column-issue_number', specific_post_row ).text(); //  remove $ sign

						// check if the Featured Product column says Yes
						
						// populate the inputs with column data
						$( '.issue-number-select option[value='+issue_number+']' ).attr('selected', 'selected');
					}
				}
			});
		</script>
    <?php
	}

	/**
   * Handles saving the meta box.
   *
   * @param int     $post_id Post ID.
   * @param WP_Post $post    Post object.
   * @return null
   */
  public function save_metabox( $post_id, $post ) {
	// if called by autosave, then bail here
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    // if this "post" post type?
    if ( $post->post_type != 'post' )
        return;

    // does this user have permissions?
    if ( ! current_user_can( 'edit_post', $post_id ) )
        return;
				if(isset($_POST['issue-number'])) {
					$issue_entity = new ACG_Issues_Entity;
					$arg = [
						'action'	=>	'u',
						'post_id'	=>	$post_id,
						'value' 	=>	$_POST['issue-number']
					];
					//print_r($arg);
					//exit();
					$issue_entity->issue_number_meta($arg);
				}
	}

	public function save_bulk_edit_hook() {
		// well, if post IDs are empty, it is nothing to do here
		if( !empty( $_GET[ 'post' ] ) ) {
			// for each post ID
			foreach( $_GET[ 'post' ] as $id ) {
				// if price is empty, maybe we shouldn't change it
				if( !empty( $_GET[ 'issue-number' ] ) ) {
					$issue_entity = new ACG_Issues_Entity;
					$issue_entity->issue_number_meta(
						[
							'action'	=>	'u',
							'post_id'	=>	$id,
							'value'		=>	isset($_GET['issue-number']) ? $_GET['issue-number']:''
						]
					);
				}
			}
		}
	}

	public function __construct()
	{
		if ( is_admin() ) {
				add_filter( 'manage_post_posts_columns', array($this, 'issues_quickedit_custom_posts_columns') );
				add_action( 'manage_post_posts_custom_column', array($this, 'issues_quickedit_custom_column_display'), 10, 2 );
				add_action( 'quick_edit_custom_box', array($this, 'issues_quickedit_fields'), 10, 2 );
				add_action( 'bulk_edit_custom_box', array($this, 'issues_quickedit_fields'), 10, 2 );
				add_action( 'admin_print_footer_scripts-edit.php', array($this, 'issues_quickedit_javascript') );
				add_filter( 'post_row_actions', array($this, 'issues_quickedit_set_data'), 10, 2);
				add_action( 'save_post', array($this, 'save_metabox'), 10, 2 );
				add_action( 'save_post', array($this, 'save_bulk_edit_hook'), 15, 2 ); 
   		 }
	}

}
