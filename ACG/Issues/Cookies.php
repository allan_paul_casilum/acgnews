<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_Cookies {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function setCurrentReaderIssue($issue_number = null, $expire = 0)
	{
		setcookie(ACG_CURRENT_ISSUE_COOKIE_PREFIX, $issue_number, $expire, "/");
	}

	public function getCurrentReaderIssue()
	{
		if( isset($_COOKIE[ACG_CURRENT_ISSUE_COOKIE_PREFIX]) ) {
			return $_COOKIE[ACG_CURRENT_ISSUE_COOKIE_PREFIX];
		}
		return false;
	}

	public function set($issue_number = null, $expire = 0)
	{
		$entity = new ACG_Issues_Entity;
		if(is_null($issue_number)) {
			$issue_number = $entity->getDefaultIssueNumber();
		}
		setcookie(ACG_COOKIE_PREFIX, $issue_number, $expire, "/");
	}

	public function get()
	{
		if( isset($_COOKIE[ACG_COOKIE_PREFIX]) ) {
			return $_COOKIE[ACG_COOKIE_PREFIX];
		}
		return false;
	}

	public function __construct()
	{

	}

}
