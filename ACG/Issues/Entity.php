<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_Entity {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function issue_number_meta($args = []){
		$prefix = ACG_ISSUE_META_PREFIX;
		if(isset($args['post_id'])) {
			$defaults = array(
				'single' => false,
				'action' => 'r',
				'value' => '',
				'prefix' => $prefix
			);
			$args = wp_parse_args( $args, $defaults );
			switch($args['action']){
				case 'd':
					delete_post_meta($args['post_id'], $args['prefix'], $args['value']);
				break;
				case 'u':
					update_post_meta($args['post_id'], $args['prefix'], $args['value']);
				break;
				case 'r':
					return get_post_meta($args['post_id'], $args['prefix'], $args['single']);
				break;
			}
		}
	}

	public function getDefaultIssueNumber()
	{
		$options = new ACG_Issues_Options;
		$post_id = $options->default_issue_homepage('r');

		$number = get_post_meta($post_id, 'number', 1);
		$publish = get_post_meta($post_id, 'publish', 1);

		if( $publish == 1 ) {
			$ret = $number;
		}else{
			$ret = 0;
		}
		return $ret;
	}

	public function getDefaultIssueDate()
	{
		$options = new ACG_Issues_Options;
		$post_id = $options->default_issue_homepage('r');

		$ret = get_post_meta($post_id, 'date', 1);
		return $ret;
	}

	public function getFormattedDateIssue($date_str, $format = "j F Y")
	{
		return date($format, strtotime($date_str));
	}

	public function __construct()
	{

	}

}
