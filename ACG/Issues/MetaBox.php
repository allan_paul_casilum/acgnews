<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_MetaBox {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	/**
   * Meta box initialization.
   */
  public function init_metabox()
	{
      add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
      add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
  }
	/**
   * Adds the meta box.
   */
  public function add_metabox()
	{
		$text_domain = acg_get_text_domain();
		add_meta_box(
			'issue-meta-box',
			__( 'Issue Meta', $text_domain ),
			array( $this, 'render_metabox' ),
			['post', 'acg-events'],
			'advanced',
			'default'
		);

  }
	/**
   * Renders the meta box.
   */
  public function render_metabox( $post )
	{
      // Add nonce for security and authentication.
      wp_nonce_field( 'issue_meta_nonce_action', 'issue_meta_nonce' );
			$data = [];
			$api = new ACG_Issues_API;
			$issue_entity = new ACG_Issues_Entity;

			$get_issue_numbers = $api->getIssuesNumberOnly();
			$data['get_issue_numbers'] = $get_issue_numbers;
			$meta = $issue_entity->issue_number_meta(
				[
					'post_id' => $post->ID,
					'action' => 'r',
					'single' => true
				]
			);
			$data['current_meta'] = $meta;
			ACG_View::get_instance()->admin_partials('partials/settings/metabox.php', $data);
  }

		/**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function save_metabox( $post_id, $post )
		{
      // Add nonce for security and authentication.
      $nonce_name   = isset( $_POST['issue_meta_nonce'] ) ? $_POST['issue_meta_nonce'] : '';
      $nonce_action = 'issue_meta_nonce_action';

      // Check if nonce is set.
      if ( ! isset( $nonce_name ) ) {
          return;
      }

      // Check if nonce is valid.
      if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
          return;
      }

      // Check if user has permissions to save data.
      if ( ! current_user_can( 'edit_post', $post_id ) ) {
          return;
      }

      // Check if not an autosave.
      if ( wp_is_post_autosave( $post_id ) ) {
          return;
      }

      // Check if not a revision.
      if ( wp_is_post_revision( $post_id ) ) {
          return;
      }
			$issue_entity = new ACG_Issues_Entity;
			$issue_entity->issue_number_meta(
				[
					'action'	=>	'u',
					'post_id'	=>	$post_id,
					'value'		=>	isset($_POST['issue-number']) ? $_POST['issue-number']:''
				]
			);
  }

	public function __construct()
	{
		if ( is_admin() ) {
			add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
			add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
    	}
	}

}
