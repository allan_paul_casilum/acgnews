<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_Get {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function __construct()
	{

	}

    public function posts()
    {
        $entity = new ACG_Issues_Entity;
        $issue_number = $entity->getDefaultIssueNumber();
		
		$argPosts = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order' => 'ASC',
			'ignore_sticky_posts' => 1,
            'meta_query' => [
                [
                    'key'     => 'issue-number',
                    'value'   => $issue_number,
                ]
            ]
		];

		$query = new WP_Query( $argPosts );

		if ( $query->have_posts() ) {
			wp_reset_postdata();
			return $query;
		}
		return false;
    }

}
