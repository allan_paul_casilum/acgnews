<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_Shortcode {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function listIssues($atts)
	{
		remove_action('pre_get_posts', 'acg_run_issues');
		$atts = shortcode_atts( array(
			'numberpost' => '-1',
			'orderby' => 'date',
			'order' => 'DESC',
		), $atts );
		$data = [];
		$arr_query = [
			'post_type' 			=> ACG_CPT_PREFIX,
			'post_status' 		=> 'publish',
			'posts_per_page' 	=> $atts['numberpost'],
			'meta_key' 				=> $atts['orderby'],
			'order' 					=> strtoupper($atts['order']),
			'orderby' 				=> 'meta_value',
			'meta_query' => [
				[
					'key' => 'publish',
					'value' => 1
				]
			]
		];
		
		$get = get_posts($arr_query);

		$options = new ACG_Issues_Options;

		$post_data = [];
		if($get) {
			foreach($get as $k => $v) {
				$meta = get_post_meta($v->ID);
				$get_default_issue = $options->default_issue_homepage('r', 0);
				$post_data[] = [
					'ID' => $v->ID,
					'post_title' => $v->post_title,
					'post_date' => $v->post_date,
					'menu_order' => $v->menu_order,
					'number' => $meta['number'][0],
					'status' => ($meta['publish'][0] == 0) ? 'Draft':'Live',
					'is_default' => ($v->ID == $get_default_issue) ? 1:0,
					'default_homepage_val' => $get_default_issue,
					'meta' => $meta
				];
			}
		}
		$data['lists'] = $post_data;
		$data['verbage'] = acg_verbage('issues');
		$data['default_issue_label'] = ACG_Settings::get_instance()->get();
		ob_start();
		ACG_View::get_instance()->admin_partials('partials/shortcodes/issues/list-issues.php', $data);
		return ob_get_clean();
	}

	private function _getCurrentIssueNumber()
	{
		remove_action('pre_get_posts', 'acg_run_issues');
		$entity = new ACG_Issues_Entity;

		$issue_number = $entity->getDefaultIssueNumber();
		if( ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue() ) {
			$issue_number = ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue();
		}
		return $issue_number;
	}

	private function _getCurrentIssueDate()
	{
		remove_action('pre_get_posts', 'acg_run_issues');
		$entity = new ACG_Issues_Entity;

		$issue_number = $entity->getDefaultIssueNumber();
		if( ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue() ) {
			$issue_number = ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue();
		}
		$args = array(
		    'posts_per_page'   => 1,
		    'post_type'        => ACG_CPT_PREFIX,
		    'meta_key'         => 'number',
		    'meta_value'       => $issue_number
		);
		$query = get_posts( $args );
		if($query) {
			$date = get_post_meta($query[0]->ID, 'date', 1);
			return $entity->getFormattedDateIssue($date);
		}
		return '';
	}

	private function _getDefaultIssueNumber()
	{
		remove_action('pre_get_posts', 'acg_run_issues');
		$entity = new ACG_Issues_Entity;

		$issue_number = $entity->getDefaultIssueNumber();
		return $issue_number;
	}

	private function _getDefaultIssueDate()
	{
		remove_action('pre_get_posts', 'acg_run_issues');
		$entity = new ACG_Issues_Entity;

		$issue_number = $entity->getDefaultIssueNumber();

		$args = array(
		    'posts_per_page'   => 1,
		    'post_type'        => ACG_CPT_PREFIX,
		    'meta_key'         => 'number',
		    'meta_value'       => $issue_number
		);
		$query = get_posts( $args );
		if($query) {
			$date = get_post_meta($query[0]->ID, 'date', 1);
			return $entity->getFormattedDateIssue($date);
		}
		return '';
	}

	public function getCurrentIssueNumber($atts)
	{

		$a = shortcode_atts( array(

		), $atts );
		$data = [];

		$issue_number = $this->_getCurrentIssueNumber();
		$verbage = acg_verbage('issues');

		return '<span class="shortcode-verbage-singular">'.$verbage['singular'].'</span> <span class="shortcode-issue-number">'.$issue_number.'</span>';
	}

	public function getCurrentIssueDate($atts)
	{

		$a = shortcode_atts( array(

		), $atts );

		$date = $this->_getCurrentIssueDate();
		return '<span class="shortcode-issue-date">'.$date.'</span>';
	}

	public function getDefaultIssueNumber($atts)
	{

		$a = shortcode_atts( array(

		), $atts );
		$data = [];
		$issue_number = $this->_getDefaultIssueNumber();
		$verbage = acg_verbage('issues');
		return '<span class="shortcode-verbage-default-singular">'.$verbage['singular'].'</span> <span class="shortcode-issue-default-number">'.$issue_number.'</span>';
	}

	public function getDefaultIssueDate($atts)
	{
		$a = shortcode_atts( array(

		), $atts );
		$data = [];

		$date = $this->_getDefaultIssueDate();
		return '<span class="shortcode-issue-default-date">'.$date.'</span>';
	}

	public function getDefaultNumberDate($atts)
	{
		$a = shortcode_atts( array(
			'seperator' => ' | '
		), $atts );
		$issue = $this->getDefaultIssueNumber($atts);
		$date = $this->getDefaultIssueDate($atts);
		return $issue . $a['seperator'] . $date;
	}

	public function getCurrentNumberDate($atts)
	{
		$a = shortcode_atts( array(
			'seperator' => ' | '
		), $atts );
		$issue = $this->getCurrentIssueNumber($atts);
		$date = $this->getCurrentIssueDate($atts);
		return $issue . $a['seperator'] . $date;
	}

	public function __construct()
	{
		add_shortcode( 'acg_show_current_issue_number', array($this, 'getCurrentIssueNumber') );
		add_shortcode( 'acg_show_current_issue_date', array($this, 'getCurrentIssueDate') );
		add_shortcode( 'acg_show_default_issue_number', array($this, 'getDefaultIssueNumber') );
		add_shortcode( 'acg_show_default_issue_date', array($this, 'getDefaultIssueDate') );
		add_shortcode( 'acg_show_default_number_date', array($this, 'getDefaultNumberDate') );
		add_shortcode( 'acg_show_current_number_date', array($this, 'getCurrentNumberDate') );
		add_shortcode( 'acg_list_issues', array($this, 'listIssues') );
	}

}
