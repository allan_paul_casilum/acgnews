<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_AJAX {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function makeDefaultIssue()
	{
		//print_r($_POST);
		$options = new ACG_Issues_Options;
		$options->default_issue_homepage('u', $_POST['post_id']);
		//set cookies current chosen issue number
		$get_issue_number = get_post_meta($_POST['post_id'], 'number', 1);
		ACG_Issues_Sessions::get_instance()->set($get_issue_number);
		wp_die();
	}

	public function removeIssue()
	{
		wp_delete_post($_POST['post_id']);
		wp_die();
	}

	public function __construct()
	{
		add_action( 'wp_ajax_make_default_issue', array($this, 'makeDefaultIssue') );
		add_action( 'wp_ajax_remove_issue', array($this, 'removeIssue') );
	}

}
