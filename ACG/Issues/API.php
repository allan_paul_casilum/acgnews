<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_API {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function get($data)
	{
		$post_id = $data['id'];
		$get = get_posts([
			'post_type' => ACG_CPT_PREFIX,
			'post_status' => 'publish',
			'posts_per_page' => 1,
			'include' => [$post_id]
		]);
		$options = new ACG_Issues_Options;
		$post_data = [];
		if($get) {
			foreach($get as $k => $v) {
				$meta = get_post_meta($v->ID);
				$get_default_issue = $options->default_issue_homepage('r', 0);
				$post_data[] = [
					'ID' => $v->ID,
					'post_title' => $v->post_title,
					'post_date' => $v->post_date,
					'menu_order' => $v->menu_order,
					'number' => $meta['number'][0],
					'status' => ($meta['publish'][0] == 0) ? 'Draft':'Live',
					'is_default' => ($v->ID == $get_default_issue) ? 1:0,
					'default_homepage_val' => $get_default_issue,
					'meta' => $meta
				];
			}
		}
		return $post_data;
	}

	public function getIssues()
	{
		$get = get_posts([
			'post_type' => ACG_CPT_PREFIX,
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'order' => 'DESC'
		]);

		$options = new ACG_Issues_Options;

		$post_data = [];
		if($get) {
			foreach($get as $k => $v) {
				$meta = get_post_meta($v->ID);
				$get_default_issue = $options->default_issue_homepage('r', 0);
				$post_data[] = [
					'ID' => $v->ID,
					'post_title' => $v->post_title,
					'post_date' => $v->post_date,
					'menu_order' => $v->menu_order,
					'number' => $meta['number'][0],
					'name' => isset($meta['name'][0]) ? $meta['name'][0] : '',
					'status' => ($meta['publish'][0] == 0) ? 'Draft':'Live',
					'is_default' => ($v->ID == $get_default_issue) ? 1:0,
					'default_homepage_val' => $get_default_issue,
					'meta' => $meta
				];
			}
		}

		return $post_data;
	}

	public function getLatestIssues()
	{
		$get = get_posts([
			'post_type' => ACG_CPT_PREFIX,
			'post_status' => 'publish',
			'posts_per_page' => 1,
			'order' => 'DESC'
		]);

		$post_data = [];
		if($get) {
			foreach($get as $k => $v) {
				$post_data[] = [
					'ID' => $v->ID,
					'post_title' => $v->post_title,
					'post_date' => $v->post_date,
					'menu_order' => $v->menu_order,
					'meta' => get_post_meta($v->ID)
				];
			}
		}

		return $post_data;
	}

	public function getIssuesNumberOnly()
	{
		$options = new ACG_Issues_Options;
		$issues = $this->getIssues();
		$issues_data = [];
		if($issues) {
			foreach($issues as $k => $v) {
				$meta = get_post_meta($v['ID']);
				$get_default_issue = $options->default_issue_homepage('r', 0);
				$issues_data[] = [
					'ID' => $v['ID'],
					'number' => $meta['number'][0],
					'date' => $meta['date'][0],
					'status' => ($meta['publish'][0] == 0) ? 'Draft':'Live',
					'is_default' => ($v['ID'] == $get_default_issue) ? 1:0,
					'default_homepage_val' => $get_default_issue,
				];
			}
		}
		return $issues_data;
	}

	public function getLiveIssues()
	{
		$get = get_posts([
			'post_type' => ACG_CPT_PREFIX,
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'order' => 'ASC',
			'meta_query' => [
				[
					'key' => 'publish',
					'value' => 1
				]
			]
		]);

		$options = new ACG_Issues_Options;

		$post_data = [];
		if($get) {
			foreach($get as $k => $v) {
				$meta = get_post_meta($v->ID);
				$get_default_issue = $options->default_issue_homepage('r', 0);
				$post_data[] = [
					'ID' => $v->ID,
					'post_title' => $v->post_title,
					'post_date' => $v->post_date,
					'menu_order' => $v->menu_order,
					'number' => $meta['number'][0],
					'status' => ($meta['publish'][0] == 0) ? 'Draft':'Live',
					'is_default' => ($v->ID == $get_default_issue) ? 1:0,
					'default_homepage_val' => $get_default_issue,
					'meta' => $meta
				];
			}
		}

		return $post_data;
	}

	public function initRestAPI()
	{
		register_rest_route( 'issues/v1', '/get-issue/(?P<id>\d+)', array(
			'methods' => 'GET',
			'callback' => array($this, 'get'),
			'args' => array(
				'id' => array(
					'validate_callback' => function($param, $request, $key) {
						return is_numeric( $param );
					}
				),
			),
		) );
		register_rest_route( 'issues/v1', '/get-issues', array(
			'methods' => 'GET',
			'callback' => array($this, 'getIssues'),
		) );
		register_rest_route( 'issues/v1', '/get-issues-number-only', array(
			'methods' => 'GET',
			'callback' => array($this, 'getIssuesNumberOnly'),
		) );
		register_rest_route( 'issues/v1', '/get-latest-issue', array(
			'methods' => 'GET',
			'callback' => array($this, 'getLatestIssues'),
		) );
		register_rest_route( 'issues/v1', '/get-live-issues', array(
			'methods' => 'GET',
			'callback' => array($this, 'getLiveIssues'),
		) );
	}

	public function __construct()
	{

	}

}
