<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_Sessions {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function setCurrentReaderIssue($issue_number = null, $expire = 0)
	{
		$_SESSION[ACG_CURRENT_ISSUE_COOKIE_PREFIX] = $issue_number;
	}

	public function getCurrentReaderIssue()
	{
		if( isset($_SESSION[ACG_CURRENT_ISSUE_COOKIE_PREFIX]) ) {
			return $_SESSION[ACG_CURRENT_ISSUE_COOKIE_PREFIX];
		}
		return false;
	}

	public function set($issue_number = null, $expire = 0)
	{
		$entity = new ACG_Issues_Entity;
		if(is_null($issue_number)) {
			$issue_number = $entity->getDefaultIssueNumber();
		}
		$_SESSION[ACG_COOKIE_PREFIX] =  $issue_number;
	}

	public function get()
	{
		if( isset($_SESSION[ACG_COOKIE_PREFIX]) ) {
			return $_SESSION[ACG_COOKIE_PREFIX];
		}
		return false;
	}

	public function init()
	{
		if(!session_id()) {
			session_start();
		}
	}

	public function __construct()
	{

	}

}
