<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Issues_IssueNumberFilter {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

    public function addDropdownFilterIssueNumber($post_type)
    {
        if('post' !== $post_type){
            return; //filter your post
        }
        if ($post_type == 'post'){
            //get unique values of the meta field to filer by.
    
            $result = allteams_query_db_issue_number();
        
            $selected = '-1';
            $request_attr = 'admin_filter_issue';
            if ( isset($_GET[$request_attr]) ) {
                $selected = $_GET[$request_attr];
            }
    
            //give a unique name in the select field
            ?><select name="admin_filter_issue">
                <option value="-1">All Issue Number</option>
                <?php 
                foreach ($result as $issue_number) {
                    $select = ($issue_number == $selected) ? ' selected="selected"':'';
                      echo '<option value="'.$issue_number.'"'.$select.'>  ' . $issue_number . ' </option>';
                }
                ?>
            </select>
            <?php
        }
    }

	public function filterByIssue($query)
	{
		global $pagenow;
		$type = 'post';
		if (isset($_GET['post_type'])) {
			$type = $_GET['post_type'];
		}
		if ( 'post' == $type && is_admin() && $pagenow=='edit.php') {
				$issue_number = '-1';
				if (isset( $_GET['admin_filter_issue'] ) && $_GET['admin_filter_issue'] != '-1')
				{
					$issue_number = (int)$_GET['admin_filter_issue'];
					$meta_query[] = array(
						'key'     => 'issue-number',
						'value'   => $issue_number,
						'compare' => '=',
						'type'    => 'NUMERIC',
					);
					$query->set( 'meta_query', $meta_query);
				}
			}
	}

	public function __construct()
	{
        add_action('restrict_manage_posts', [$this, 'addDropdownFilterIssueNumber'], 10, 1);
		add_filter('parse_query', [$this, 'filterByIssue'], 25, 1 );
	}

}
