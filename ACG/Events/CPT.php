<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Events_CPT {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function cpt() {
		$t = acg_get_plugin_details();
		$name = acg_verbage('settings_menu');
		$text_domain = acg_get_text_domain();

		$labels = array(
			'name'               => _x( 'Events', 'post type general name', $text_domain ),
			'singular_name'      => _x( 'Events', 'post type singular name', $text_domain ),
			'menu_name'          => _x( 'Events', 'admin menu', $text_domain ),
			'name_admin_bar'     => _x( 'Events', 'add new on admin bar', $text_domain ),
			'add_new'            => _x( 'Add New', 'events', $text_domain ),
			'add_new_item'       => __( 'Add New Events', $text_domain ),
			'new_item'           => __( 'New Events', $text_domain ),
			'edit_item'          => __( 'Edit Events', $text_domain ),
			'view_item'          => __( 'View Events', $text_domain ),
			'all_items'          => __( 'All Events', $text_domain ),
			'search_items'       => __( 'Search Events', $text_domain ),
			'parent_item_colon'  => __( 'Parent Events:', $text_domain ),
			'not_found'          => __( 'No Events found.', $text_domain ),
			'not_found_in_trash' => __( 'No Events found in Trash.', $text_domain )
		);

		$args = array(
			'labels'             => $labels,
	    'description'        => __( 'Description.', $text_domain ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => ACG_EVENTS_CPT_PREFIX ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'menu_icon'					 => 'dashicons-calendar-alt',
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);
		register_post_type( ACG_EVENTS_CPT_PREFIX, $args );
	}

	public function __construct()
	{
		$this->cpt();
	}

}
