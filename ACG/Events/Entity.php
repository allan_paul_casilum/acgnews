<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Events_Entity {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function date_meta($args = []){
		$prefix = 'acg_event_date_meta';
		if(isset($args['post_id'])) {
			$defaults = array(
				'single' => false,
				'action' => 'r',
				'value' => '',
				'prefix' => $prefix
			);
			$args = wp_parse_args( $args, $defaults );
			switch($args['action']){
				case 'd':
					delete_post_meta($args['post_id'], $args['prefix'], $args['value']);
				break;
				case 'u':
					update_post_meta($args['post_id'], $args['prefix'], $args['value']);
				break;
				case 'r':
					return get_post_meta($args['post_id'], $args['prefix'], $args['single']);
				break;
			}
		}
	}

	public function title_meta($args = []){
		$prefix = 'acg_event_title_meta';
		if(isset($args['post_id'])) {
			$defaults = array(
				'single' => false,
				'action' => 'r',
				'value' => '',
				'prefix' => $prefix
			);
			$args = wp_parse_args( $args, $defaults );
			switch($args['action']){
				case 'd':
					delete_post_meta($args['post_id'], $args['prefix'], $args['value']);
				break;
				case 'u':
					update_post_meta($args['post_id'], $args['prefix'], $args['value']);
				break;
				case 'r':
					return get_post_meta($args['post_id'], $args['prefix'], $args['single']);
				break;
			}
		}
	}

	public function __construct()
	{

	}

}
