<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 *
 * @since 0.0.1
 * */
class ACG_Events_Shortcode {
  /**
	 * instance of this class
	 *
	 * @since 0.0.1
	 * @access protected
	 * @var	null
	 * */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     0.0.1
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function listEvents($atts)
	{
		remove_action('pre_get_posts', 'acg_run_issues');
		$a = shortcode_atts( array(
			'numberpost' 	=> 5,
		), $atts );
		$entity = new ACG_Issues_Entity;
		$issue_number = $entity->getDefaultIssueNumber();
		if( ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue() ) {
			$issue_number = ACG_Issues_Sessions::get_instance()->getCurrentReaderIssue();
		}

		$data = [];
		$get = get_posts([
			'post_type' => ACG_EVENTS_CPT_PREFIX,
			'post_status' => 'publish',
			'posts_per_page' => $a['numberpost'],
			'order' => 'DESC',
			'meta_query' => array(
					array(
						'key'     => 'issue-number',
						'value'   => $issue_number,
					),
				)
		]);

		$post_data = [];

		$event_entity = new ACG_Events_Entity;

		if($get) {
			foreach($get as $k => $v) {
				/*
				* Date Format
				* https://www.w3schools.com/php/func_date_date_format.asp
				*/
				$date_time = $event_entity->date_meta([
					'post_id'	=> $v->ID,
					'single'	=> true,
				]);
				$post_data[] = [
					'ID' => $v->ID,
					'post_title' => $v->post_title,
					'post_date' => $v->post_date,
					'menu_order' => $v->menu_order,
					'date_event' => $date_time,
					'title_event' => $event_entity->title_meta([
						'post_id'	=> $v->ID,
						'single'	=> true,
					]),
					'meta' => get_post_meta($v->ID)
				];
			}
		}
		$data['lists'] = $post_data;
		ob_start();
		ACG_View::get_instance()->admin_partials('partials/shortcodes/events/list-events.php', $data);
		return ob_get_clean();
	}

	public function __construct()
	{
		add_shortcode( 'acg_list_events_by_issues', array($this, 'listEvents') );
	}

}
