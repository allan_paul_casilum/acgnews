<div class="bootstrap-iso">
  <div class="wrap issue-metabox">
  <div class="form-row">
    <div class="form-group col-md-2">
      <label for="issuenumber">Number</label>
      <input type="text" name="number" value="<?php echo $number;?>" class="form-control form-control-sm" id="issuenumber">
    </div>
    <div class="form-group col-md-2">
      <label for="issueName">Name</label>
      <input type="text" name="name" value="<?php echo $name;?>" class="form-control form-control-sm" id="issuename">
    </div>
    <div class="form-group col-md-2">
      <label for="issueDate">Date</label>
      <input type="text" name="date" value="<?php echo $date;?>" class="form-control form-control-sm" id="issueDate">
    </div>
    <div class="form-group col-md-2">
      <label for="issuePublish">Publish</label>
      <select class="form-control form-control-sm" name="publish">
        <option value="0" <?php echo ($publish == 0) ? 'selected':'';?>>Draft</option>
        <option value="1" <?php echo ($publish == 1) ? 'selected':'';?>>Live</option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="issuePublish">Make Default Issue</label>
      <select class="form-control form-control-sm" name="default_issue_query">
        <option value="0" <?php echo ($is_default_issue_number == 0) ? 'selected':'';?>>No</option>
        <option value="1" <?php echo ($is_default_issue_number == 1) ? 'selected':'';?>>Yes</option>
      </select>
    </div>
  </div>
  </div>
  <script>
jQuery(document).ready( function () {
  jQuery('#issueDate').datepicker({
    dateFormat: 'yy-mm-dd'
  });
});
</script>
</div>
