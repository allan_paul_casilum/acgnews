<form name="create" action="<?php echo $action;?>" method="post" autocomplete="off">
  <input type="hidden" name="_method" value="<?php echo $method;?>">
  <div class="form-row">
    <div class="form-group col-md-2">
      <label for="issuenumber">Number</label>
      <input type="text" name="number" value="<?php echo ($latest_issue['number'] + 1);?>" class="form-control form-control-sm" id="issuenumber">
    </div>
    <div class="form-group col-md-2">
      <label for="issueName">Name</label>
      <input type="text" name="name" value="<?php echo (isset($latest_issue['name']) ? $latest_issue['name'] : '');?>" class="form-control form-control-sm" id="issuename">
    </div>
    <div class="form-group col-md-2">
      <label for="issueDate">Date</label>
      <input type="text" name="date" class="form-control form-control-sm" id="issueDate">
    </div>
    <div class="form-group col-md-2">
      <label for="issuePublish">Publish</label>
      <select class="form-control form-control-sm" name="publish">
        <option value="0">Draft</option>
        <option value="1">Live</option>
      </select>
    </div>
  </div>

  <button type="submit" class="btn btn-primary">Save New Issue</button>
</form>

<?php ACG_View::get_instance()->admin_partials('partials/settings/issue-settings.php', $data); ?>

<script>
jQuery(document).ready( function () {
  jQuery('#issueDate').datepicker({
    dateFormat: 'yy-mm-dd'
  });
});
</script>
