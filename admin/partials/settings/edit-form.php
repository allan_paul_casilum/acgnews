<form name="create" action="<?php echo $action;?>" method="post" autocomplete="off">
  <input type="hidden" name="_method" value="<?php echo $method;?>">
  <div class="form-row">
    <div class="form-group col-md-2">
      <label for="issuenumber">Number</label>
      <input type="text" name="number" value="<?php echo $qData[0]['meta']['number'][0];?>" class="form-control form-control-sm" id="issuenumber">
    </div>
    <div class="form-group col-md-2">
      <label for="issueName">Name</label>
      <input type="text" name="name" value="<?php echo ( isset($qData[0]['meta']['name'][0]) ? $qData[0]['meta']['name'][0] : '');?>" class="form-control form-control-sm" id="issuename">
    </div>
    <div class="form-group col-md-2">
      <label for="issueDate">Date</label>
      <input type="text" name="date" value="<?php echo $qData[0]['meta']['date'][0];?>" class="form-control form-control-sm" id="issueDate">
    </div>
    <div class="form-group col-md-2">
      <label for="issuePublish">Publish</label>
      <select class="form-control form-control-sm" name="publish">
        <option value="0" <?php echo ($qData[0]['meta']['publish'][0] == 0) ? 'selected':'';?>>Draft</option>
        <option value="1" <?php echo ($qData[0]['meta']['publish'][0] == 1) ? 'selected':'';?>>Live</option>
      </select>
    </div>
  </div>
  <input type="hidden" name="ID" value="<?php echo $qData[0]['ID'];?>">
  <button type="submit" class="btn btn-primary">Update</button>
</form>
<script>
jQuery(document).ready( function () {
  jQuery('#issueDate').datepicker({
    dateFormat: 'yy-mm-dd'
  });
});
</script>
