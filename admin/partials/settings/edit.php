<div class="bootstrap-iso">
  <div class="wrap issue-wrapper">
    <h3>Edit</h3>
    <?php ACG_View::get_instance()->admin_partials('partials/settings/edit-form.php', ['action'=>$action,'method'=>$method,'qData'=>$qData]); ?>
  </div>
</div>
