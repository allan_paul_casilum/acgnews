<form name="create" action="<?php echo $action;?>" method="post" autocomplete="off">
  <input type="hidden" name="_method" value="save-settings">
  <p>Settings</p>
  <div class="form-row">
    <div class="form-group col-md-2">
      <label for="defaultLabel">Default Label</label>
      <select class="form-control form-control-sm" name="defaultLabel" id="defaultLabel">
        <option value="number" <?php echo ($issue_default_label == 'number') ? 'selected':''; ?>>Number</option>
        <option value="name" <?php echo ($issue_default_label == 'name') ? 'selected':''; ?>>Name</option>
      </select>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Save Settings</button>
</form>
