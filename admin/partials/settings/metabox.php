<div class="bootstrap-iso">
  <div class="wrap issue-metabox">
    <h3>Choose Issue #</h3>
    <pre>
      <?php //print_r($get_issue_numbers);?>
    </pre>
    <?php if($get_issue_numbers) { ?>
          <select name="issue-number" class="form-control form-control-sm">
            <option value="-1">Select Issue Number</option>
            <?php foreach($get_issue_numbers as $k=>$v) { ?>
              <option value="<?php echo $v['number'];?>" <?php echo ($v['number']==$current_meta) ? 'selected':'';?>>Issue - <?php echo $v['number'];?> - <?php echo $v['status'];?> <?php echo ($v['default_homepage_val']==$v['ID']) ? ' - Current Default Issue':'';?></option>
            <?php } ?>
          </select>
    <?php } ?>
  </div>
</div>
