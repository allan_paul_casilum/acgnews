<div class="bootstrap-iso">
  <div class="wrap issue-wrapper">
    <h3>Create Issue</h3>
    <?php ACG_View::get_instance()->admin_partials('partials/settings/create-form.php', ['action'=>$action,'method'=>$method]); ?>
  </div>
</div>
