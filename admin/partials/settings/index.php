<div class="bootstrap-iso">
  <div class="wrap issue-wrapper">
    <div class="create-issues">
      <?php ACG_View::get_instance()->admin_partials('partials/settings/create-form.php', $data); ?>
    </div>
    <div class="list-issues">
      <?php ACG_View::get_instance()->admin_partials('partials/settings/list-issues.php', ['latest_issue' => $latest_issue, 'issues' => $issues]); ?>
    </div>
  </div>
</div>
