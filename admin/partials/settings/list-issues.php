<div class="table-responsive">
<table id="table_list_issues" class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Issue #</th>
            <th>Name</th>
            <th>Date</th>
            <th>Status</th>
            <th>Default Issue?</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>ID</td>
            <td>Issue #</td>
            <td>Name</td>
            <td>Date</td>
            <td>Status</td>
            <td>Default Issue?</td>
            <td>Action</td>
        </tr>
    </tbody>
</table>
</div>
<script>
jQuery(document).ready( function () {
    var _admin_url = '<?php echo acg_admin_url();?>';
    var _public_url = '<?php echo acg_public_url();?>';
    var table = jQuery('#table_list_issues').DataTable({
      "ajax": {
        "url" : rest_object.acg_api_url + "get-issues",
        "dataSrc": ""
      },
      columns: [
         { data: 'ID' },
         { data: 'meta.number' },
         { data: 'name' },
         { data: 'meta.date' },
         { data: 'status' },
         {
           data: 'is_default',
           render: function ( data, type, row ) {
              var html_text;
              if(data == 1) {
                html_text = 'Yes';
              }else{
                html_text = '<a href="#" class="btn btn-primary btn-sm make-this-default">Make this Default</a> - <a href="'+_public_url+'/?issue-id='+row.number+'&preview-issue=1" target="_blank" class="btn btn-primary btn-sm">Preview</a>';
              }
              return html_text;
           }
         },
      ],
      "order": [[ 1, "desc" ]],
      "columnDefs": [
        {
          "targets": 0,
          "visible": false,
          "searchable": false
        },
        {
          "targets": 6,
          "data": null,
          "defaultContent": '<a href="#" class="btn btn-primary btn-sm edit-issue">Edit</a> <a href="#" class="btn btn-primary btn-sm remove-issue">Remove</a>'
        }
      ]
    } );
    jQuery('#table_list_issues tbody').on( 'click', '.make-this-default', function (e) {
        e.preventDefault();
        var data = table.row( jQuery(this).parents('tr') ).data();
        console.log(data.ID);
        var data = {
      		'action': 'make_default_issue',
      		'post_id': data.ID
      	};
      	jQuery.post(ajaxurl, data, function(response) {
      		table.ajax.reload();
      	});
    } );
    jQuery('#table_list_issues tbody').on( 'click', '.edit-issue', function (e) {
        e.preventDefault();
        var data = table.row( jQuery(this).parents('tr') ).data();
        console.log(data.ID);
        window.location = rest_object.acg_admin_url + '&_method=edit&id=' + data.ID;
    } );
    jQuery('#table_list_issues tbody').on( 'click', '.remove-issue', function (e) {
        e.preventDefault();
        var data = table.row( jQuery(this).parents('tr') ).data();
        var data = {
      		'action': 'remove_issue',
      		'post_id': data.ID
      	};
      	jQuery.post(ajaxurl, data, function(response) {
      		table.ajax.reload();
      	});
    } );
} );
</script>
