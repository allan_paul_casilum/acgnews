<div class="bootstrap-iso">
  <div class="acg-list-events">
    <ul class="events-list">
      <?php if($lists) { ?>
          <?php foreach($lists as $k=>$v) { ?>
              <li class="acg-events-<?php echo $v['ID'];?>">
                <strong><?php echo $v['date_event'];?></strong> <?php echo $v['post_title'];?>
              </li>
          <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
