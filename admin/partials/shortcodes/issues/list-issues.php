<div class="bootstrap-iso">
  <div class="acg-list-issues">
    <ul>
      <?php if($lists) { ?>
          <?php foreach($lists as $k=>$v) { ?>
              <li class="news-issue-item" id="issue-<?php echo $v['ID'];?>">
                <a href="<?php echo site_url('?issue='.$v['meta']['number'][0].'');?>">
                  <?php echo $verbage['singular'];?>
                  <?php
                    if($default_issue_label == 'number'){
                      echo $v['meta']['number'][0];
                    }else{
                      echo isset($v['meta']['name'][0]) ? $v['meta']['name'][0] : '';
                    }
                  ?>
                  | <?php echo ACG_Issues_Entity::get_instance()->getFormattedDateIssue($v['meta']['date'][0]);?>
                </a>
              </li>
          <?php } ?>
      <?php } ?>
    </ul>
  </div>
</div>
